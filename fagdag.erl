-module(fagdag).
-export([content/0]).

content()->
	[
	{block,	[
		{big,"Kort om"},
		{formfeed,1},
		{big,"Erlang"},
		{formfeed,2},
		{normal,"Hvordan det kan gi slik avsindig driftssikkerhet?"},
		{line,80},
		{normal,"Oyvind Skaar, fagdag november 2012."}
		]
	},

	{block,[
		{big,"Historikk"},
		{formfeed, 1},
		{normal, "Wikipedia: The name 'Erlang' has been understood as a reference to Danish mathematician and engineer "},
		{normal, "Agner Krarup Erlang, and (initially at least) simultaneously as an abbreviation of 'Ericsson Language'."},
		{formfeed,2},
		{bullet, "Laget av Ericson i 1986"},
		{bullet, "Joe Armstrong ledet arbeidet med et sprak som skulle kjore på telefon-switcher"},
		{bullet, "Spraket ble open-sourcet i 1998"}
	]},

	{big, "Hello world"},
	
	{block,[
		{big,"Implementasjoner"},
		{formfeed, 1},
		{line,80},
		{formfeed, 3},
		{bullet,"CouchDB"},
		{bullet,"Riak"},
		{bullet,"Facebook chat"},
		{bullet,"GitHub"},
		{bullet,"WhatsApp"}
	]},
	
	{block,[
		{big, "Variabler"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Stor forbokstav"},
		{bullet,"Underscore om verdien skal ignoreres"},
		{bullet,"Kan ikke endres nar verdien er satt"}
	]},

	{block,[
		{big, "Types"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Atom"},
		{bullet,"Tuple"},
		{bullet,"List"},
		{bullet,"Fun"}
	]},


	{block,[
		{big, "Pattern"},
		{formfeed,1},
		{big, "matching"}
	]},

	{block,[
		{big, "List"},
		{formfeed,1},
		{big, "comprehension"}
	]},

	{block,[
		{big, "Recursion"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Et tilbakevendende pattern i Erlang."},
		{bullet,"Tail recursion optimization"}
	]},

	{block,[
		{bullet,"Ports"},
		{bullet,"BIFs & NIFs"},
		{bullet,"VM"},
		{bullet,"Garbage collection"}
	]},

	{block,[
		{big, "Prosesser"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Isolation scope"},
		{bullet,"Billig (ca 1KB)"},
		{bullet,"Clustering og distribusjon er innebygget i spraket"},
		{bullet,"Round-robin concurrency"},
		{bullet,"Impliserer Actor-pattern"}
	]},

	{block,[
		{big, "Meldinger"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Alle prosesser har en postboks"},
		{bullet,"Ingen meldingsgaranti"},
		{bullet,"Meldinger leses i en receive-block"}
	]},

	{big, "Noder"},

	{big, "Hot code swap"},

	{block,[
		{big, "Error handling"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"Links"},
		{bullet,"Signals"},
		{bullet,"Supervisors"}
	]},

	{block,[
		{big, "Tools"},
		{formfeed,1},
		{big, "Platforms"},
		{formfeed,1},
		{line,80},
		{formfeed, 3},
		{bullet,"OTP"},
		{bullet,"Mnesia"},
		{bullet,"Dialyzer"},
		{bullet,"Eunit"}
	]},
	
	{file,"logo.txt"}
	].