-module(renderer).
-export([render/1]).

render({big,Text})->
	util:map(fun(X)->output(X) end,alphabet:text(Text));
render({line,Length})->
	output(line(Length,[]));
render({file,Path})->
	 for_each_line_in_file(Path,fun(X)-> outputinline(X) end, [read], 0);
render({bullet,Text})-> output(lists:concat(["*  " ,Text]));
render({normal,Text})-> output(Text);
render({group, Group})->
	renderGroup(Group);
render({formfeed,0})->
	ok;
render({formfeed,Count})->
	output(""),
	render({formfeed,Count-1});
render({block,Content})->
	render({formfeed,5}),
	render({line,80}),
	render({formfeed,1}),
	render({group,Content}),
	render({formfeed,2});

render(Text)-> output(Text).

line(0,Content)->Content;
line(Length,Content)->line(Length-1,["*"|Content]).

renderGroup([])->
	ok;
renderGroup([Current|Rest])->
	render(Current),
	renderGroup(Rest).

for_each_line_in_file(Name, Proc, Mode, Accum0) ->
    {ok, Device} = file:open(Name, Mode),
    for_each_line(Device, Proc, Accum0).

for_each_line(Device, Proc, Accum) ->
    case io:get_line(Device, "") of
        eof  -> file:close(Device), Accum;
        Line -> NewAccum = Proc(Line),
                    for_each_line(Device, Proc, NewAccum)
    end.

output(Text)->
	outputter:writeline(Text).
	
outputinline(Text)->
	outputter:writeline(Text).