1. Kompiler alle filene
2. Start ouput i den noden du vil ha det ut i med `outputter:start().`
3. Start presentasjonen i den noden du vil kjøre det fra med `P=presenter:start(fagdag:content()).`
4. Naviger med 
    * `P ! next.`
    * `P ! prev.`
    * `P ! current.`
    * `P ! 3.` (for å gå til element 3)
    * `P ! stop.`
