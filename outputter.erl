-module(outputter).
-export([start/0,stop/0,write/1,writeline/1,listen/0]).
-define(SERVER,presentation_outputter).

start()->
	global:trans({?SERVER,?SERVER},
		fun()->
			case global:whereis_name(?SERVER) of
				undefined->
					Pid=spawn(outputter,listen,[]),
					global:register_name(?SERVER,Pid);
				_->
					ok
				end
			end).
stop()->
global:trans({?SERVER,?SERVER},
	fun()->
		case global:whereis_name(?SERVER) of
			undefined ->
				ok;
			_->
				global:send(?SERVER,shutdown)
			end
		end).

write(Text)->
	global:send(?SERVER,{inline,Text}).
writeline(Text)->
	global:send(?SERVER,{line,Text}).

listen()->
	receive
		{line,Text}->
			io:format("~s~n",[Text]),
			listen();
		{inline,Text}->
			io:format("~s~n",[Text]),
			listen();
		shutdown ->
			exit;
		_->
			io:format("Unknown text~n"),
			listen()
		end.
