-module(presenter).
-export([start/1,load/2]).

start(Presentation)->
	spawn(?MODULE,load,[[],Presentation]).

load(Past,Future)->
	receive
		next when Future /= [] ->
			[Current|Rest]=Future,
			render(Current),
			load(lists:append(Past,[Current]),Rest);
		prev when length(Past) > 1 ->
			{Rest,[Current|[Next|_]]}=lists:split(length(Past)-2,Past),
			render(Current),
			load(lists:append(Rest,[Current]),[Next|Future]);
		current when Past /= [] ->
			{_,Current}=lists:split(length(Past)-1,Past),
			render(Current),
			load(Past,Future);
		current ->
			[Current|_]=Future,
			render(Current),
			load(Past,Future);
		rewind->
			load([],lists:concat([Past,Future]));
		SlideNo when SlideNo > 0, SlideNo =< length(Past)+length(Future)->
			{Before,[Current|After]}=lists:split(SlideNo-1,lists:concat([Past,Future])),
			render(Current),
			load(lists:append(Before,[Current]),After);
		swap->
			?MODULE:load(Past,Future);
		exit->
			exit(normal)
	after 6000000 ->
		render(line),
		timeout
	end.

render(Page)->renderer:render(Page).